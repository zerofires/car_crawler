from entry.ICar import ICar
from entry.Car import Car
from utils.LogUtil import LogUtil

# 启用日志
LogUtil.init("car.log", True)


def face():
  title = '懂车帝官网多线程爬虫'
  width = 140
  print('#' * width)
  print('##', " " * (width - 6), '##')
  print('##', " " * (width - 6), '##')
  print('##', title.center(width - len(title) * 2 + 4), '##')
  print('##', " " * (width - 6), '##')
  print('##', " " * (width - 6), '##')
  print('#' * width)
  LogUtil.info(title="多线程爬虫启动")


def main():
  car: ICar = Car('config.json')
  car.start()


def finish():
  LogUtil.info(title="爬虫程序已终止")


if __name__ == "__main__":
  face()
  main()
  finish()

#%%
from importlib import import_module
import os
import sys

sys.path.append("..")

print(os.getcwd())
print(sys.path)
import utils.CarUtil
from utils.LogUtil import LogUtil
from utils.timmer import timmer
import json

#%%

# %%
dict1 = { '1': [ '东', '西', '南', '北', '中'], '2': 'hello'}
dict2 = {
    '1': [
        '东',
        '西',
        '南',
        '北',
    ],
    '3': [
        '书',
        '香',
        '门',
        '第',
    ],
    '4': 'world'
}

dict3 = dict(dict1)
print(dict3 is dict1)

print(type(dict1).__name__)
print(dict1, dict2)

dict1.update(dict2)
print(dict1)

#%%
print(type(True).__name__)

setting = { '1': 3}
setting = None
assert setting is not None, 'setting is None'
# print(len(setting))

assert 1 == 1, "yes"
assert 1 == 2, "no"
# %%
# os模块使用测试
import os

print(os.path.exists('config.json'))
print(os.path.isfile('config.json'))
print(os.getcwd())

# %%
# 数据插入测试
import sys
from pathlib import Path

current_folder = Path(__file__).absolute().parent
father_folder = str(current_folder.parent)
sys.path.append(father_folder)

from store.StoreFactory import StoreFactory as sf

storeConfig = {
    "type": "mysql",
    "settings":
        {
            "mysql":
                {
                    "host": "127.0.0.1",
                    "port": 3306,
                    "user": "root",
                    "password": "root",
                    "db": "car_data",
                    "charset": "utf8"
                },
            "mssql": {}
        }
}

dataService = sf.getStoreService(storeConfig)
brands = dataService.get_brands()
print(brands)

# data = ('1','test_品牌','/hello/world')
# d.insert("insert into car_brand (brand_id,brand_name,path) values (%s,%s,%s)",data)

dic = {
    'sub_brand_id':
        20,
    'sub_brand_name':
        '一汽-大众奥迪',
    'series_url':
        'https://www.dongchedi.com/auto/series/96/model-54760',
    'series_id':
        96,
    'series_name':
        96,
    'img_url':
        'https://p1-dcd.byteimg.com/img/motor-img/b9d9856f7ddc210dc2d7a5e20f3055a4~noop.png',
    'year':
        2022,
    'car_id':
        54760,
    'car_name':
        '35 TFSI 时尚动感型',
    'official_price':
        '30.68万',
    'engine_description':
        '2.0T 150马力 L4',
    'acceleration_time':
        '9.7s',
    'fuel_comprehensive':
        '6.2L/100km',
    'front_suspension_form_description':
        '多连杆式独立悬挂'
}

data = tuple(dic.values())
print(data)
# dataService.insert_info(( dic, 5 ))

#%%

import os

p = os.path.join('http://www.baidu.com', '/hello/world/')
print(p)


def m(*args, **kwargs):
  print("m: ", args, kwargs)
  pass


def m2(*args, **kwargs):
  print("m2: ", *args, kwargs)
  m(*args, **kwargs)
  pass


a = [ 1, 2, 3 ]

b = { 'c': '4', 'd': '5'}

m2(*a)


def m3(a, b, c):
  print(a, b, c)
  pass


print(*b)
# m3(b)

#%%

a = None
print(str(a))

d = ( 1, 2, 3 )
print((*d, 4))
one, *_ = d
print(one, _)

str = "update car_info set `sub_brand_id`=%s, `sub_brand_name`=%s, `series_url`=%s, `series_id`=%s, `series_name`=%s, `img_url`=%s, `year`=%s, `car_id`=%s, `car_name`=%s, `official_price`=%s, `engine_description`=%s, `acceleration_time`=%s, `fuel_comprehensive`=%s, `front_suspension_form_description`=%s, `rear_suspension_form_description`=%s, `seat_material`=%s, `oil_tank_volume`=%s, `engine_max_power`=%s, `fuel_form_description`=%s, `driven_form_description`=%s, `wheelbase`=%s, `max_speed`=%s, `engine_max_torque`=%s, `gearbox_description`=%s, `fourwheel_drive_type_description`=%s, `air_control_model`=%s, `curb_weight`=%s, `seat_count`=%s, `baggage_volume`=%s,, "
print(str.removesuffix(', '))
print('\n', str.rstrip(', '))

#%%
tmp = id, name, age, intro = 10, "hello", 18, "this is a test."
id, *_ = tmp       # 这样表示只获取第一个值
_, name, *_ = tmp  # 这样表示只获取第二个值
*_, name, _ = tmp  # 这样表示只获取第三个值
                   # 注意事项 *_ 只允许出现一次, 而 _ 可以出现多次, 比如 *_, name, *_ 这样语法是错的
                   # 此处可不看: 解构是按元组排序进行赋值的, 像 _, _, name, _ 与 *_, age, _ 是一样的
print(id, name)

#%%
import typing
from utils.CarUtil import CarUtil


@typing.overload
def get_page(page: str) -> int:
  print('hello')
  pass


@typing.overload
def get_page(page: str, name: str) -> int:
  print('hello', name)
  pass


# 一定要定义一个没有装饰器的函数
def get_page(page, name=None) -> int:
  return page, name


print(get_page(12))
print(get_page(12, "nfds"))
print(type(get_page('12')))

###############################################################################

#%%
#region 模块测试
from utils.LogUtil import LogUtil

LogUtil.init("test.log", True)
modules = [
    {
        "name": "ListPageModule",
        "enable": True
    },
    {
        "name": "ConfigPageModule",
        "enable": False
    }
]
if modules:
  for module in modules:
    try:
      name = module.get('name')
      # 根据配置查看是否启用此模块
      if module.get("enable"):
        module_name = f'modules.{name}'
        LogUtil.info(f"启动模块【{module_name}】")
        mod = import_module(module_name)
        # mod.start(self)
    except Exception as ex:
      LogUtil.info(f"【{name}】启动失败 - {str(ex)}")
else:
  LogUtil.info("没有选中任何模块")

#endregion

# 存储服务测试

#%%
# 功能类测试

#region LogUtil

from utils.LogUtil import LogUtil

LogUtil.init("test.log", True)
LogUtil.info("hello")

                   #endregion

                   # 整合测试

                   # %%

from abc import ABCMeta, abstractmethod, abstractproperty
from typing import Dict

from store.IStoreService import IStoreService
from utils.CarHttp import CarHttp


class ICar(metaclass=ABCMeta):

  @abstractproperty
  def config(self) -> Dict:
    """返回当前配置信息

    Returns:
        Dict: 当前配置信息
    """
    pass

  @config.setter
  def config(self, value: Dict):
    """自定义配置，按配置文件要求来

    Args:
        value (Dict): 一个字典类型的配置信息
    """
    pass

  @abstractproperty
  def http(self) -> CarHttp:
    """获取http请求接口

    Returns:
        CarHttp: http请求接口
    """
    pass

  @abstractproperty
  def storeService(self) -> IStoreService:
    """获取存储服务

    Returns:
        IStoreService: 存储服务接口
    """
    pass

  @abstractproperty
  def mode(self) -> str:
    """运行模式

    Returns:
        str: DEBUG、TEST、PRODUCT
    """
    pass

  @abstractmethod
  def initConfig(self, path: str) -> Dict:
    """根据路径加载配置文件

    Args:
        path (str): 配置文件路径

    Returns:
        Dict: 返回配置信息
    """
    pass

  @abstractmethod
  def initStoreService(
      self,
      storeService: IStoreService = None
  ) -> IStoreService:
    """初始化数据存储服务接口

    Args:
        storeService (IStoreService, optional): 数据存储服务接口. Defaults to None.

    Returns:
        IStoreService: 数据存储服务接口
    """
    pass

  @abstractmethod
  def start(self):
    """启动应用
    """
    pass

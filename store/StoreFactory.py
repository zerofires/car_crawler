from store.IStoreService import IStoreService
from store.mysql.MySqlStoreService import MySqlStoreService


class StoreFactory:

  @staticmethod
  def getStoreService(storeConfig={}) -> IStoreService:
    # 数据存储服务
    storeService: IStoreService = None
    # 数据库名
    storeType = storeConfig.get("type", "mysql").lower()
    storeSetting = storeConfig.get("settings").get(storeType)

    # 根据配置获取相应的数据存储服务
    if storeType == 'mysql':
      storeService = MySqlStoreService(storeSetting)
      pass
    #endif
    elif storeType == 'mssql':

      pass
    #endif
    elif storeType == 'csv':

      pass
    #endif
    return storeService
from abc import ABCMeta, abstractmethod


class IStoreService(metaclass=ABCMeta):

  def __init__(self) -> None:
    pass

  @abstractmethod
  def get_brands(self, *args, **kwargs):
    """获取所有的品牌
    """
    pass

  @abstractmethod
  def get_series(self,  count: int | None = None, *args, **kwargs):
    """获取所有的车型
    """
    pass

  @abstractmethod
  def insert_seriesId(self, *data):
    """插入一个车型信息并返回插入id

    Args:
        data (_type_): _description_
    """
    pass

  @abstractmethod
  def insert_brand(self, data):
    """插入一个品牌并返回插入id

    Args:
        data (_type_): _description_
    """
    pass

  @abstractmethod
  def insert_info(self, data):
    """修改一个车型信息并返回影响行数

    Args:
        data (_type_): _description_
    """
    pass

  @abstractmethod
  def get_sql_by_update_dict(self, table, dic: dict, condition: str):
    """通过字典数据生成一个修改sql语句

    Args:
        table (str): 表名
        dic (dict): 数据字典
        condition (str): 条件
    """
    pass
